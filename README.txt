
Module: tarteaucitron.js
Authors: Amauri CHAMPEAUX (http://amauri.champeaux.fr)


Description
===========
Intégration du service de mise en conformité tarteaucitron.js

Requirements
============
1. Ouvrir un compte premium sur https://opt-out.ferank.eu/pro/

Installation
============
1. Unpack the 'tarteaucitron' folder and contents in the appropriate modules
   directory of your Drupal installation.  This is probably
   'sites/all/modules/'
   
2. Enable the tarteaucitron.js module in the administration tools,
   it is classified in "Statistics".
   
Usage
=====
1. To add services, go to
   'Home > Administration > Config > System > tarteaucitron.js'
